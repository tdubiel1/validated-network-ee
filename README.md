# validated-network-ee
The following Code was used for the WEBINAR "Use network automation to manage your multivendor networks". This demo used Ansible Validated Content for Netwoks and leveraged the Ansible Event Driven Automation. Please note this demo used both EDA CLI and EDA Controller from AAP version 2.4.

## Validated Content 
The following blog does a great job of explianing the network.base validated content.
https://www.ansible.com/blog/based-validated-network-content

* In the demo we used the resource manager role with the action of persist to gather the existing configurations (resource modules) and save them as host_vars/ yaml files.

* We also used the action deploy to push the configurations from the host_vars to the spare router.

* Please note, some conditionals and templates were also used to configure tunnel destinations that were not covered by current resource modules.

## Getting started with AAP 2.4 and the EDA Controller

Download AAP 2.4:
https://developers.redhat.com/products/ansible/download

Install Guide:
https://access.redhat.com/documentation/en-us/red_hat_ansible_automation_platform/2.4/html/red_hat_ansible_automation_platform_installation_guide/index


## Getting started with EDA CLI, Telegraf and  Kafka
The following links will explain how to install EDA CLI and the other components used in the demo to receive streaming telemetry from the Cisco routers.

### Event-Driven Ansible install:
Please note this code is the developer tech preview.
https://github.com/ansible/event-driven-ansible

### Telegraf Collector Install
https://docs.influxdata.com/telegraf/v1.21/introduction/installation/

Please see this repo for my example telegraf.toml file.

### Kafka Install
https://kafka.apache.org/quickstart


## Taxonomy of the Demo
### Playbooks
* Job-Template (network-persist)- playbook (2-persist.yml) (Gathers existing device configd and saves as yaml config files)
* Job-Template (Spare-router) - playbook (cisco_spare.yml)
*                                -roles:
*                                       aws (bring up spare router)
*                                       validated (push failed router config to spare)
*                                       health (ensure OSPF and BGP work on spare)
### Rulebooks
* router_check - EDA CLI to listen to EDA topic from Telegraf (streaming telemetry) runs playbook to send data as webhook to EDA Controller
* router_message - EDA Controller - listen on :5000 for web hooks and runs job template Spare-router if failed router condition received. 

